# ReMyth

A matrix.org bot built using matrix-nio library

## Description

Since I wanted to make a Matrix bot, I can code Python (as a beginner) and I'm tired with current Discord and what happened to discord.py, this bot was made.

## Features

+ Detect prefixes and commands
+ E2E support (as matrix-nio is)
+ Very unoptimized and bloated code
+ Dynamic commands system! (When you use `commands.Command` to create a command, the command will be automatically registered)
+ Bot commands (last updated 18/11/2021):
    + echo - Say the thing you want me to say!
    + waifu2x - Upscale images (Anime-like art preferred) using Waifu2x.
    + img2pdf - Convert images to a PDF document.
    + ytdl - Get download info for various video and audio sites, powered by Youtube-DL
    + ping - Get the bot current latency when connecting to the homeserver
    + latency - Alias for ping command
gfwtest - Check if a website is blocked from GFW
    + math - Math commands because why not?
    + translate - Translate text to a language, using Google Translate
    + libretranslate - Translate text to a language, using LibreTranslate
    + tr - Alias for translate command
    + ltr - Alias for libretranslate command

## Badges

[![Built with matrix-nio](https://img.shields.io/badge/built%20with-matrix--nio-brightgreen)](https://github.com/poljar/matrix-nio)

## Installation

### Warning

+ You can't enable E2E support in Windows due to libolm not available in Windows, in that case you can use matrix-js-sdk to get started.
+ In Linux, you have to find the `libolm` package to get E2E support in your distro package manager (e.g `libolm` in Arch Linux)
+ If your distro package manager include python packages, you should install from it instead of `pip`

### Progress

1. Clone this repo: `git clone https://gitlab.com/tretrauit/remyth-matrix/`
2. Install dependencies: `pip install -r requirements.txt`
2.1. You may have to uninstall `nio` pip package if you have installed matrix-nio from your package manager.
3. Run the bot: `python ./src/`

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support

+ For issues, please open an issue
+ For anything else, you can join the matrix room at [#remyth-development:matrix.org](https://matrix.to/#/#remyth-development:matrix.org) or DM me at @tretrauit:matrix.org

## Roadmap

+ Fix shlex parser crash when parsing a string with a single quote character
+ Refactor the code
+ Add more features
+ Focus on study QwQ (My score low as heck)

## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

+ [poljar](https://github.com/poljar) for his awesome [matrix-nio](https://github.com/poljar/matrix-nio) library, without it this bot (and other bots) wouldn't be possible.
+ [nio-template](https://github.com/anoadragon453/nio-template) for giving me the core idea on how to use matrix-nio.
+ [matrix-commander](https://github.com/8go/matrix-commander) for their encrypted attachment decryption code, without it this project wouldn't have full E2E support.

## License

See [LICENSE](./LICENSE) or tl;dr then it's Apache License 2.0

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

