from callbacks import Callbacks
from pathlib import Path
from nio import *

class Bot(AsyncClient):
    def write_credentials(self, resp: LoginResponse, homeserver):
        config = self.bot_config
        config["bot"]["credentials"]["homeserver"] = homeserver
        config["bot"]["credentials"]["user_id"] = resp.user_id
        config["bot"]["credentials"]["device_id"] = resp.device_id
        config["bot"]["credentials"]["access_token"] = resp.access_token
        self.cfgutil.save(config)

    def __init__(self, cfgutil, logger):
        self.cfgutil = cfgutil
        self.bot_config = cfgutil.config
        self.logger = logger
        client_config = AsyncClientConfig(
            max_limit_exceeded=0,
            max_timeouts=0,
            store_sync_tokens=True,
            encryption_enabled=self.bot_config["bot"]["e2e_encryption"]
        )
        user_id = self.bot_config["bot"]["user"]["user_id"]
        store_path = self.bot_config["bot"]["store_path"]
        homeserver = self.bot_config["bot"]["credentials"]["homeserver"]
        device_id = self.bot_config["bot"]["credentials"]["device_id"]

        self.logger.debug("Creating store and temporary directory...")
        Path(store_path).mkdir(exist_ok=True, parents=True)
        Path(self.bot_config["bot"]["tmp_path"]).mkdir(exist_ok=True, parents=True)

        if not (homeserver.startswith("https://") or homeserver.startswith("http://")):
            homeserver = "https://" + homeserver
            self.bot_config["bot"]["credentials"]["homeserver"] = homeserver
            self.cfgutil.save()
        super().__init__(homeserver=homeserver, user=user_id, store_path=store_path, config=client_config, device_id=device_id)
        self.logger.info("Registering callbacks (Unencrypted)...")
        callbacks = Callbacks(self, self.bot_config, self.logger)
        self.add_event_callback(callbacks.cb_parse, (RoomMessageText, RoomMessageImage,))
        self.add_event_callback(callbacks.invite, (InviteMemberEvent,))
        self.add_event_callback(callbacks.unknown, (UnknownEvent,))
        self.add_to_device_callback(callbacks.key_verify, (KeyVerificationEvent,))
        # We get this when we receive message in an encrypted room and the bot is unencrypted.
        self.add_event_callback(callbacks.decryption_failure, (MegolmEvent,))
        if self.bot_config["bot"]["e2e_encryption"]:
            logger.info("Registering callbacks (Encrypted)...")
            self.add_event_callback(callbacks.cb_parse, (RoomEncryptedImage,))
        logger.info("Sucessfully registered.")

    async def login_server(self):
        access_token = self.bot_config["bot"]["credentials"]["access_token"]
        if access_token == "":
            logger.debug("Access token not found, falling back to user login mode.")
            device_name = self.bot_config["bot"]["user"]["device_name"]
            homeserver = self.bot_config["bot"]["credentials"]["homeserver"]
            password = self.bot_config["bot"]["user"]["password"]
            resp = await self.login(password=password, device_name=device_name)

            # check that we logged in succesfully
            if (isinstance(resp, LoginResponse)):
                self.write_credentials(resp, homeserver)
                self.logger.info(f"Logged in using userinfo: {self.user_id} on {self.device_id}")
            else:
                logger.error(f"Failed to log in: {resp}")
        else:
            self.restore_login(user_id=self.bot_config["bot"]["user"]["user_id"], 
            device_id=self.bot_config["bot"]["credentials"]["device_id"], access_token=access_token)
            self.load_store()
            self.logger.info(f"Logged in using stored credentials: {self.user_id} on {self.device_id}")
