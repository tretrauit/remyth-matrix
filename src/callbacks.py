from parser import Parser
import colorlog
import traceback
import base64

from utilities import Utilities
from nio import *

class Callbacks:
	# We use JSON and the builitn JSON <-> dict so the config is a dictionary
	def __init__(self, client: AsyncClient, config: dict, logger):
		"""
		Args:
			client: nio client used to interact with matrix.
			store: Bot storage.
			config: Bot configuration parameters.
		"""
		self.client = client
		self.config = config
		self.command_prefixes = config["bot"]["prefixes"]
		self.logger = logger
		self.utils = Utilities(client, config, logger)
		# We create a new parser for every room and every user when they chat 
		# (quite memory wasting and this isn't as smart as Discord.py)
		self.uni_parser = {}
		self.parser = None
		self.logger.debug("Initialized Callbacks")

	async def cb_parse(self, room: MatrixRoom, event):
		"""Parse callbacks and call the correct callbacks.
		Args:
			room: The room the event came from.
			event: The event defining the message.
		"""
		if event.sender == self.client.user:
			return

		uni_sender = base64.b64encode((event.sender + room.machine_name).encode("utf-8")).decode("utf-8")
		if uni_sender not in self.uni_parser:
			self.uni_parser[uni_sender] = Parser(self.client, self.config, self.logger)
			self.logger.debug("Created a parser for {}".format(uni_sender))
		self.parser = self.uni_parser.get(uni_sender)
		if self.config["bot"]["log_level"] == "debug":
			self.logger.debug(f"Message received for room '{room.display_name}' from '{room.user_name(event.sender)}' ({str(type(event))})")
			self.logger.debug(event.source)
		if type(event) == RoomMessageText:
			await self.text_message(room, event)
		if type(event) == RoomMessageImage or type(event) == RoomEncryptedImage:
			await self.image_message(room, event)

	async def image_message(self, room: MatrixRoom, event: RoomMessageImage or RoomEncryptedImage):
		# Make the parser parse the image for us.
		await self.parser._set_cache("image", event)

	async def text_message(self, room: MatrixRoom, event: RoomMessageText):
		# Extract the message text
		msg = event.body
		# Process as message if in a public room without command prefix
		given_prefixes = [p for p in self.command_prefixes if msg.startswith(p)]
		# If it features a command prefix, treat it as a command
		if given_prefixes and len(given_prefixes) == 1:
			prefix = given_prefixes[0]
			self.logger.debug(f"Detected prefix: %s", prefix)
			# Remove the command prefix
			msg = msg[len(prefix):]
			self.logger.debug("The message has a command prefix!, parsed command: %s", msg)
			await self.parser._process(room, event, prefix, msg)

	async def key_verify(self, event):  # noqa
		"""Handle events sent to device."""
		try:
			client = self.client
			if isinstance(event, KeyVerificationStart):  # first step
				if "emoji" not in event.short_authentication_string:
					self.logger.debug("Other device does not support emoji verification "
						  f"{event.short_authentication_string}.")
					return
				resp = await client.accept_key_verification(
					event.transaction_id)
				if isinstance(resp, ToDeviceError):
					self.logger.debug(f"accept_key_verification failed with {resp}")

				sas = client.key_verifications[event.transaction_id]

				todevice_msg = sas.share_key()
				resp = await client.to_device(todevice_msg)
				if isinstance(resp, ToDeviceError):
					print(f"to_device failed with {resp}")

			elif isinstance(event, KeyVerificationCancel):  # anytime
				# There is no need to issue a
				# client.cancel_key_verification(tx_id, reject=False)
				# here. The SAS flow is already cancelled.
				# We only need to inform the user.
				self.logger.debug(f"Verification has been cancelled by {event.sender} "
					  f"for reason \"{event.reason}\".")

			elif isinstance(event, KeyVerificationKey):  # second step
				sas = client.key_verifications[event.transaction_id]
				self.logger.debug(f"{sas.get_emoji()}")
				resp = await client.confirm_short_auth_string(
					event.transaction_id)
				if isinstance(resp, ToDeviceError):
					self.logger.error(f"confirm_short_auth_string failed with {resp}")

			elif isinstance(event, KeyVerificationMac):  # third step
				sas = client.key_verifications[event.transaction_id]
				try:
					todevice_msg = sas.get_mac()
				except LocalProtocolError as e:
					# e.g. it might have been cancelled by ourselves
					self.logger.debug(f"Cancelled or protocol error: Reason: {e}.\n"
						  f"Verification with {event.sender} not concluded. "
						  "Try again?")
				else:
					resp = await client.to_device(todevice_msg)
					if isinstance(resp, ToDeviceError):
						print(f"to_device failed with {resp}")
					self.logger.debug(f"sas.we_started_it = {sas.we_started_it}\n"
						  f"sas.sas_accepted = {sas.sas_accepted}\n"
						  f"sas.canceled = {sas.canceled}\n"
						  f"sas.timed_out = {sas.timed_out}\n"
						  f"sas.verified = {sas.verified}\n"
						  f"sas.verified_devices = {sas.verified_devices}\n")
					self.logger.debug("Emoji verification was successful!\n"
						  "Hit Control-C to stop the program or "
						  "initiate another Emoji verification from "
						  "another device or room.")
			else:
				self.logger.debug(f"Received unexpected event type {type(event)}. "
					  f"Event is {event}. Event will be ignored.")
		except BaseException:
			self.logger.exception(traceback.format_exc())

	async def invite(self, room: MatrixRoom, event: InviteMemberEvent) -> None:
		self.logger.debug(f"Got invite to {room.room_id} from {event.sender}.")
		# Attempt to join 3 times before giving up
		for attempt in range(3):
			result = await self.client.join(room.room_id)
			if type(result) == JoinError:
				self.logger.error(
					f"Error joining room {room.room_id} (attempt %d): %s",
					attempt,
					result.message,
				)
			else:
				break
		else:
			self.logger.error("Unable to join room: %s", room.room_id)

		# Successfully joined room
		self.logger.info(f"Joined {room.room_id}")

	async def decryption_failure(self, room: MatrixRoom, event: MegolmEvent):
		self.utils.trust_devices(event.sender, event.device_id)
		# await self.client.start_key_verification(event)
		self.logger.error(f"Decryption failure for event '{event.event_id}' in room '{room.room_id}'")

	async def unknown(self, room: MatrixRoom, event: UnknownEvent) -> None:
		"""Callback for when an event with a type that is unknown to matrix-nio is received.
		Currently this is used for reaction events, which are not yet part of a released
		matrix spec (and are thus unknown to nio).
		Args:
			room: The room the reaction was sent in.
			event: The event itself.
		"""
		# if event.type == "m.reaction":
		# 	# Get the ID of the event this was a reaction to
		# 	relation_dict = event.source.get("content", {}).get("m.relates_to", {})

		# 	reacted_to = relation_dict.get("event_id")
		# 	if reacted_to and relation_dict.get("rel_type") == "m.annotation":
		# 		await self._reaction(room, event, reacted_to)
		# 		return

		self.logger.debug(
			f"Got unknown event with type to {event.type} from {event.sender} in {room.room_id}."
		)
