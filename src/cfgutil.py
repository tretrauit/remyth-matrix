import json
from pathlib import Path

class cfgutil():
    def get_default_config(self):
        return {
            "bot": {
                "user": {
                    "user_id": "",
                    "password": "",
                    "device_name": "ReMyth",
                },
                "credentials": {
                    "homeserver": "",  # e.g. "https://matrix.example.org"
                    "user_id": "",  # e.g. "@user:example.org"
                    "device_id": "",  # device ID, 10 uppercase letters
                    "access_token": ""  # cryptogr. access token
                },
                "auto_reconnect": True,
                "loglevel": "debug",
                "e2e_encryption": True,
                "store_path": "./store/",
                "tmp_path": "./tmp/",
                "prefixes": [
                    "!"
                ]
            },
            "waifu2x": {
                "ncnn-vulkan-exec": "waifu2x-ncnn-vulkan",
                "converter-cpp": "",
                "caffe-exec": ""
            }
        }

    def __init__(self, file = "./config.json"):
        self.config_file = Path(file)
        if self.config_file.exists():
            with Path(file).open("r+") as f:
                self.config = json.load(f)
        else:
            self.config = self.get_default_config()
            self.save()

    def save(self, config = None):
        if config is None:
            config = self.config
        with self.config_file.open("w") as f:
            json.dump(config, f, indent=4)
