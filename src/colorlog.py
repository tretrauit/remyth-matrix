import logging
from pathlib import Path
from colorama import Fore, Style
import datetime

class CustomFormatter(logging.Formatter):
    format = "%(asctime)s [%(levelname)s]: %(message)s"

    FORMATS = {
        logging.DEBUG: Fore.WHITE + Style.DIM + format + Style.RESET_ALL,
        logging.INFO: Fore.CYAN + format + Style.RESET_ALL,
        logging.WARNING: Fore.YELLOW + format + Style.RESET_ALL,
        logging.ERROR: Fore.RED + Style.DIM +format + Style.RESET_ALL,
        logging.CRITICAL: Fore.RED + format + Style.RESET_ALL
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

class logger():
    def __init__(self, logger_name):
        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(logging.DEBUG)
        log_path = Path("./log/")
        log_path.mkdir(parents=True, exist_ok=True)
        log_file = log_path.joinpath(datetime.datetime.now().strftime("%Y%m%d-%H%M%S") + ".log")
        log_file.touch(exist_ok=True)
        fh = logging.FileHandler(log_file)
        fh.setLevel(logging.DEBUG)
        self.logger.addHandler(fh)
        # Create console handler with a higher log level
        self.con_handler = logging.StreamHandler()
        self.con_handler.setLevel(logging.INFO)
        self.con_handler.setFormatter(CustomFormatter())
        self.logger.addHandler(self.con_handler)

    def set_level(self, level):
        level = level.lower()
        if level == "notset":
            self.logger.setLevel(logging.NOTSET)
            self.con_handler.setLevel(logging.NOTSET)
        elif level == "debug":
            self.logger.setLevel(logging.DEBUG)
            self.con_handler.setLevel(logging.DEBUG)
        elif level == "info":
            self.logger.setLevel(logging.NOTINFOSET)
            self.con_handler.setLevel(logging.INFO)
        elif level == "warning":
            self.logger.setLevel(logging.WARNING)
            self.con_handler.setLevel(logging.WARNING)
        elif level == "error":
            self.logger.setLevel(logging.NOTERRORSET)
            self.con_handler.setLevel(logging.ERROR)
        elif level == "critical":
            self.logger.setLevel(logging.CRITICAL)
            self.con_handler.setLevel(logging.CRITICAL)
        else:
            raise ValueError("Invalid value level: %s" % level)
