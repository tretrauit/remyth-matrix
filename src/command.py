commands = []

def get_command(command: str, hidden = False):
    for v in commands:
        if command.startswith(v.name) and (hidden or not v.hidden):
            return v

class _Command():
    def __init__(xelf, function, name = None, description = None, hidden = False, parent = None):
        # We can't use self because functions in parser.py use self
        # Wrap self to function so we can do child command.
        xelf.Command = xelf.Command
        xelf.function = function
        xelf.name = name
        xelf.description = description
        xelf.commands = [] # Subcommands.
        xelf.cache = None
        xelf.hidden = hidden # Prevent user from executing the command
        if parent: # If we're making a subcommand.
            parent.commands.append(xelf)
        else:
            commands.append(xelf)

    # Copy the outer function but slightly different.
    def Command(xelf, function = None, name = None, description = None, hidden = False):
        if name is None:
            name = function.__name__
            
        if xelf.get_command(name) is not None:
            raise NameError(f"'{name}' command is already defined.")

        def wrapper(function):
            return _Command(function, name, description, hidden, xelf)
        wrapper.name = name
        wrapper.description = description
        # Wrap self to function so we can do subcommands.
        wrapper.Command = Command
        return wrapper

    def get_command(xelf, command: str):
        for v in xelf.commands:
            if command.startswith(v.name):
                return v

    async def execute_child(xelf, command, self = None, room = None, event = None, utils = None, args = None, prefix = None):
        cmd = xelf.get_command(command)
        if cmd is not None:
            kwargs = {
                "self": self,
                "room": room,
                "event": event,
                "utils": utils,
                "args": args,
                "prefix": prefix
            }
            await cmd.function(**kwargs)
        else:
            return -1      

    async def __call__(xelf, **kwargs):
        return await xelf.function(**kwargs)
        # Here the code returning the correct thing.

# wrap _Command to allow for deferred calling
def Command(function = None, name = None, description = None, hidden = False):
    if get_command(name) is not None:
        raise NameError(f"'{name}' command is already defined.")

    def wrapper(function):
        return _Command(function, name, description, hidden=hidden)
    wrapper.name = name
    wrapper.description = description
    # Wrap self to function so we can do subcommands.
    wrapper.Command = Command
    return wrapper