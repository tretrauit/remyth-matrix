import time
import command
import yt_dlp
import aiohttp
import json
import shlex
from freetranslate import GoogleWebTranslate, GoogleTranslate, LibreTranslate, BingTranslator
from nio import DownloadError
from aiopath import AsyncPath
from PIL import Image
from io import BytesIO

config, cfgobj, logger = None, None, None
gtrwebclient, gtrclient, libreclient, bingclient = None, None, None, None
def initialize(raw_config, raw_logger):
    global config
    global cfgobj
    global logger
    config = raw_config.config
    cfgobj = raw_config
    logger = raw_logger
    global bingclient
    global gtrwebclient
    global libreclient
    global gtrclient
    # Or Google TRanslate web client
    gtrwebclient = GoogleWebTranslate()
    libreclient = LibreTranslate()
    gtrclient = GoogleTranslate()
    bingclient = BingTranslator()

@command.Command(name="echo", description="Say the thing you want me to say!")
async def _echo(utils, args, **kwargs):
    """Echo back the command's arguments"""
    response = " ".join(args)
    await utils.send_message(response)

@command.Command(name="waifu2x", description="Upscale images (Anime-like art preferred) using Waifu2x.")
async def _waifu2x(utils, args, cache, name, **kwargs):
    def info():
        name = _waifu2x.name
        description = _waifu2x.description
        usage = "#### Usage:\n\
+ With no arguments, it'll catch your previous/later sent image and upscale it.\n\
+ `cancel` - Cancel the upscaling process if you have triggered it.\n\
+ `help` - Show this help."
        return name, description, usage
    if config["waifu2x"]["ncnn-vulkan-exec"] == "":
        await utils.send_message("#### Error\n\
Waifu2x is disabled for this bot.")
        return
    if len(args) > 0: # Check if command has arguments
        if args[0] == "cancel":
            if name == _waifu2x.name:
                await utils.send_message("Upscaling image using waifu2x cancelled.")
        elif args[0] == "help":
            await utils.send_message(utils.gen_help(*info()))
    else: # No command arguments we know.
        if cache is None:
            await utils.send_message("##### Please send a image to upscale below.")
            return ("request_cache", "image")
        else:
            rsp = await utils.download_file(cache)
            logger.debug(rsp)
            if type(rsp) == DownloadError:
                await utils.send_message("##### Failed to download image.")
            else:
                logger.debug("Writing image to disk...")
                file_name = f"./tmp/{utils.gen_random_str(32)}"
                async with AsyncPath(f"{file_name}.tmp").open("wb") as f:
                    await f.write(rsp.body)
                    await f.flush()
                logger.debug("Executing Waifu2x...")
                await utils.execute_process(config["waifu2x"]["ncnn-vulkan-exec"], "-i", 
                f"{file_name}.tmp", "-n", "3", "-o", f"{file_name}.png")
                if await AsyncPath(f"{file_name}.png").exists():
                    logger.debug("Upscaling completed.")
                    await utils.send_image(file_name + ".png")
                else:
                    logger.debug("Upscaling failed.")
                    await utils.send_message("##### An error occured while upscaling the image.")

@command.Command(name="img2pdf", description="Convert images to a PDF document.")
async def _img2pdf(cache, utils, args, prefix, name, **kwargs):
    def info():
        name = _img2pdf.name
        description = _img2pdf.description
        usage = "#### Usage:\n\
+ With no arguments, it'll catch your images until you use `img2pdf start` to convert.\n\
+ `start` - Convert the images you've sent to a PDF document.\n\
+ `cancel` - Cancel the converting process if you have triggered it.\n\
+ `help` - Show this help."
        return name, description, usage

    def clear():
        _img2pdf.cache = None

    if len(args) > 0:
        if args[0] == "start" and isinstance(_img2pdf.cache, list):
            logger.debug(f"Cache length: {len(_img2pdf.cache)}")
            if len(_img2pdf.cache) < 1:
                await utils.send_message(f"#### Error\n\
You probably want to make PDF from at least images, try sending \
more images or `{prefix}img2pdf cancel` to cancel.")
            else:
                imgs = []
                logger.debug("Reading images...")
                for v in _img2pdf.cache:
                    rsp = await utils.download_file(v)
                    logger.debug(rsp)
                    if type(rsp) == DownloadError:
                        await utils.send_message("Failed to download a image.")
                        return
                    try:
                        img = Image.open(BytesIO(rsp.body))
                        if img.format == "PNG" and len(img.split()) == 4: #RGBA, we need to remove Alpha channel since PDF sucks.
                            img.load()
                            post_img = Image.new("RGB", img.size, (255, 255, 255))
                            logger.debug(img.split())
                            post_img.paste(img, mask=img.split()[3]) # 3 is the alpha channel
                            img = post_img
                        imgs.append(img)
                    except:
                        await utils.send_message("Couldn't read image file, conversion failed.")
                        return

                file_name = f"./tmp/{utils.gen_random_str(32)}.pdf"
                logger.debug("Writing PDF to: {}".format(file_name))
                imgs[0].save(file_name, "PDF" ,resolution=100.0, save_all=True, append_images=imgs[1:])
                await utils.send_file(file_name)
                await utils.send_message("#### Notice\n\
This function will be deprecated soon, thank you for using my software :D")
                clear()
        elif args[0] == "cancel":
            if name == _img2pdf.name:
                await utils.send_message("Conversion cancelled.")
        elif args[0] == "help":
            await utils.send_message(utils.gen_help(*info()))
    else:
        if _img2pdf.cache is None:
            _img2pdf.cache = []
            await utils.send_message(f"Please send images you want to convert to PDF documents, \
when you're done execute `{prefix}img2pdf start` to start converting.\n\
##### NOTE: The image must not be large or wrong orientation may happen (You can uncheck 'Send image \
in original resolution' in Element mobile to fix that)")
        else:
            _img2pdf.cache.append(cache)
            logger.debug("Image added to 'img2pdf' cache")
        return "request_cache", "image" # We both need image cache anyway

@command.Command(name="ytdl", description="Get download info for various video and audio sites, powered by Youtube-DL")
async def _ytdl(utils, args, **kwargs):
    def info():
        name = _ytdl.name
        description = _ytdl.description
        usage = "#### Usage:\n\
+ [urls] - Get download info for all URLs specified\n\
+ Example: `ytdl https://www.youtube.com/watch?v=dQw4w9WgXcQ https://www.youtube.com/watch?v=grd-K33tOSM`\n\
+ `search` [keyword] - Search video on Youtube and return the result.\n\
+ Example: `ytdl search Amogus trap remix`\n\
+ `help` - Show this help.\n\
##### Notes:\n\
+ You CAN'T use `search` with [urls] together.\n\
+ This bot CAN'T generate a DL for YouTube with both **video** and **audio** combined, because this bot only \
fetch DL but **not download** and **re-encode** them because how the YouTube system works and the limitation \
of the currently hosting machine"
        return name, description, usage
    if len(args) > 0:
        out_text = ""
        if args[0] == 'help':
            await utils.send_message(utils.gen_help(*info()))
            return
        if args[0] == "search":
            search = " ".join(args[1:])
            logger.debug(f"Trying to parse information for '{search}' (search mode)")
            try:
                media_info, best_audio, best_video = utils.ytdl_fetch_info(search, search=True)
                out_text += f'#### Download URL generated for [{media_info["title"]}]({media_info["webpage_url"]})\n'
                if best_audio is not None:
                    out_text += f'[[Audio: {best_audio["format"]}]]({best_audio["url"]}) '
                if best_video is not None:
                    out_text += f'[[Video: {best_video["format"]}]]({best_video["url"]}) '
                out_text += "\n"
            except (yt_dlp.utils.DownloadError, KeyError) as ex:
                logger.exception(f'Error downloading using YoutubeDL', ex)
                out_text += f"##### An error occurred while fetching searching for '{search}'\n" 
            finally:
                return
        for v in args:
            if v:
                logger.debug(f"Trying to parse information for '{v}'")
                try:
                    media_info, best_audio, best_video = utils.ytdl_fetch_info(v)
                    out_text += f'#### Download URL generated for [{media_info["title"]}]({v})\n'
                    if best_audio is not None:
                        out_text += f'[[Audio: {best_audio["format"]}]]({best_audio["url"]}) '
                    if best_video is not None:
                        out_text += f'[[Video: {best_video["format"]}]]({best_video["url"]}) '
                    out_text += "\n"
                except (yt_dlp.utils.DownloadError, KeyError) as ex:
                    logger.exception(f'Error downloading using YoutubeDL', ex)
                    out_text += f"##### An error occurred while fetching info for '{v}'\n"
        logger.debug(f"Successfully fetch information for '{args}', sending to user...")
        out_text += ">If this is a YouTube URL then the video download will\
        NOT contain sound, you'll need to download both and merge them together (For more info check `ytdl help` command)."
        await utils.send_message(out_text)
        return
    await utils.send_message(utils.gen_help(*info()))

@command.Command(name="ping", description="Get the bot current latency when connecting to the homeserver")
async def _ping(event, utils, **kwargs):
    latency = int(time.time() * 1000) - event.server_timestamp
    await utils.send_message(f"#### Pong!\n\
Current latency: {latency}ms\n\
>Note: This only check the latency between the **bot** and the **server**")

@command.Command(name="latency", description="Alias for `ping` command")
async def _latency(event, utils, **kwargs):
    await _ping(event=event, utils=utils, **kwargs)

@command.Command(name="gfwtest", description="Check if a website is blocked from GFW")
async def _gfwtest(args, utils, **kwargs):
    if len(args) == 0:
        await utils.send_message("#### Error\n\
Please send at least one URL to check.")
        return

    msg = ""
    async with aiohttp.ClientSession() as session:
        for v in args:
            msg += f"#### Test result for `{v}`\n"
            requests_count, ok_request = 0, 0
            try:
                async with session.get("https://www.guru99.com/tools/china.php", params={"domain": v}) as response:
                    rsp_json = json.loads(await response.text()) # Workaround for text/html response type
                    requests_count = len(rsp_json["server"])
                    for sv in rsp_json["server"]:
                        if sv["resultstatus"] == "ok":
                            ok_request += 1
                    msg += f"{ok_request}/{requests_count} successful requests. {rsp_json['description']}\n"
            except aiohttp.ClientError as ex:
                logger.exception(f"Error while fetching GFW test result for '{v}'", ex)
                msg += f"##### An error occurred while fetching GFW test result for '{v}'\n"

    msg += ">Powered by https://www.guru99.com/tools/china-firewall-test-tool.html, for proper test please use https://web-tester.appinchina.co"
    await utils.send_message(msg)

@command.Command(name="math", description="Math commands because why not?")
async def _math(utils, args, **kwargs):
    if len(args) > 0:
        if args[0].lower() == "help":
            await command.get_command("help")(utils=utils, args=args[1:], base_command=_math, **kwargs)
            return
        result = await _math.execute_child(args[0], utils=utils, args=args[1:], **kwargs)
        if result == -1:
            await utils.send_message("Unknown command for `math`")
        return
    logger.debug("Math function implementation is dummy.")

async def _translate(utils, args, engine):
    if len(args) < 3:
        await utils.send_message("#### Error\n\
Please set source language, target language and the text to translate to.")
        return
    tr_engine = None
    if engine == "googleweb":
        tr_engine = gtrwebclient
    elif engine == "google":
        tr_engine = gtrclient
    elif engine == "libre":
        tr_engine = libreclient
    elif engine == "bing":
        tr_engine = bingclient
    else:
        raise NotImplementedError(f"Unknown translation engine '{engine}'")
    tr_str = " ".join(args[2:])
    logger.debug(tr_str)
    try:
        tr_result = await tr_engine.translate(tr_str, args[1], args[0])
    except RuntimeError:
        await utils.send_message("#### Error\n\
An error occurred while translating the text.")
    else:
        await utils.send_message(f"""#### Translation result
##### Source language: `{tr_result.original_lang}`
##### Translated: 
{tr_result.translated_text}
>Translated with [freetranslate](https://pypi.org/project/freetranslate/) library, using {tr_engine.__class__.__name__} module.""")

@command.Command(name="translate", description="Translate text to a language, using Google Translate")
async def _translate_google(utils, args, **kwargs):
    await _translate(utils=utils, args=args, engine="google")

@command.Command(name="gwebtranslate", description="Translate text to a language, using Google Translate website")
async def _translate_google_web(utils, args, **kwargs):
    await _translate(utils=utils, args=args, engine="googleweb")

@command.Command(name="bingtranslate", description="Translate text to a language, using Bing Translator")
async def _translate_bing(utils, args, **kwargs):
    await _translate(utils=utils, args=args, engine="bing")

@command.Command(name="libretranslate", description="Translate text to a language, using LibreTranslate")
async def _translate_libre(utils, args, **kwargs):
    await _translate(utils=utils, args=args, engine="libre")

@command.Command(name="tr", description="Alias for `translate` command")
async def _translate_google_alias(args, utils, **kwargs):
    await _translate_google(args=args, utils=utils, **kwargs)

@command.Command(name="gwtr", description="Alias for `gwebtranslate` command")
async def _translate_google_web_alias(args, utils, **kwargs):
    await _translate_google_web(args=args, utils=utils, **kwargs)

@command.Command(name="btr", description="Alias for `bingtranslate` command")
async def _translate_bing_alias(args, utils, **kwargs):
    await _translate_bing(args=args, utils=utils, **kwargs)

@command.Command(name="ltr", description="Alias for `libretranslate` command")
async def _translate_libre_alias(args, utils, **kwargs):
    await _translate_libre(args=args, utils=utils, **kwargs)

@_math.Command(name="plus", description="plus!")
async def _plus(args, **kwargs):
    logger.debug("Plus function for math implementation is dummy.")

@_math.Command(name="delta", description="delta!")
async def _math_delta(utils, args, **kwargs):
    floatargs = []
    try:
        for v in args:
            floatargs.append(float(v))
        result = floatargs[1]**2 - 4 * floatargs[0] * floatargs[2]
        await utils.send_message(str(result))
    except Exception as e:
        logger.debug("Failed to do delta operation, ", e)
        await utils.send_message("Failed to do delta operation.")

