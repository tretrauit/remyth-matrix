import json
import colorlog
import asyncio
import traceback
import commands
from nio.exceptions import *
from aiohttp import ClientConnectionError, ServerDisconnectedError
from colorama import init, deinit
from bot import Bot
from cfgutil import cfgutil

async def main() -> None:
    """Main function for the bot to run"""
    logobj = colorlog.logger(__name__)
    # Make logger accessible to exec() function
    global logger
    logger = logobj.logger
    try:
        logger.info("Reading configuration file...")
        cfgobj = cfgutil()
    except json.decoder.JSONDecodeError:
        logger.error("Invalid configuration file, please edit the file or delete it to force the app generate a new one.")
        return
    else:
        config = cfgobj.config
        # Set log level immediately after reading the configuration file.
        logobj.set_level(config["bot"]["log_level"])
        # Credential with password & access token hidden.
        obf_conf = config | {
            "bot": {
                "user": {
                    "password": "<null>"
                },
                "credentials": {
                    "access_token": "<null>"
                }
            }
        }
        logger.debug(f"Configuration file: {obf_conf}")
        if config == cfgobj.get_default_config():
            logger.error("Please edit the default configuration file and try again.")
            return
        # Set logging level
        bot = Bot(cfgobj, logger)
        commands.initialize(cfgobj, logger)
        try:
            await bot.login_server()
        except LocalProtocolError as e:
            logger.fatal(
                "Failed to login. Have you installed the correct dependencies? "
                "https://github.com/poljar/matrix-nio#installation "
                "Error: %s",
                e,
            )
        if bot is None:
            logger.error("Failed to login to the server.")
            return
        if config["bot"]["e2e_encryption"]: # encryption enabled.
            if bot.should_upload_keys:
                logger.info("Syncing encryption key with the server...")
                await bot.keys_upload()
                logger.info("Synced sucessfully.")
        try:
            await bot.sync_forever(set_presence="online", full_state=True)
        except KeyboardInterrupt:
            pass
        except (ClientConnectionError, ServerDisconnectedError) as ex:
            logger.exception("Failed to sync with the server", ex)
        finally:
            logger.debug("Disconnecting bot...")
            await bot.close()
            logger.info("Bot disconnected.")

# Execute the main function
def exec():
    init() # Initialize colorama (mostly for Windows)
    try:
        asyncio.get_event_loop().run_until_complete(main())
    except KeyboardInterrupt:
        pass
    except Exception as e:
        e_msg = f"An error occurred while the bot was running.\n{e}\n{traceback.print_exc()}"
        if logger is None:
            # Falling back to print function
            print(e_msg)
        else:
            logger.fatal(e_msg)
    finally:
        deinit()
    
# Call Main function if we execute this file
if __name__ == "__main__":
    exec()
