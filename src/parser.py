import shlex
import command
from nio import AsyncClient
from utilities import Utilities

class Parser:
    # We want to use this for "caches" so modify the parser from the example bot.
    def __init__(self, client: AsyncClient, config: dict, logger):
        """
        A command parser, crappy but it works.
        """
        self.client = client
        self.config = config
        self.logger = logger
        self.caches = {
            "image": [None] * 2 + [False],
            "media": [None] * 2 + [False],
        }

    async def _set_cache(self, cache_type, event):
        cache = self.caches[cache_type]
        if cache[2]:
            cache[0] = event
            old_cache = cache
            await self._process(**(cache[1] | {'cache': event}))
            if old_cache == cache:
                # If the cache remain unchanged then we delete it.
                cache = [None] * 2 + [False]
                self.logger.debug(f"{cache} cleaned.")      

    async def _process(self, room, event, prefix, raw_cmd, cache = None, parent_command = None):
        # We should use kwargs to pack multiple arguments in commands (for easier reading)
        lexer = shlex.shlex(raw_cmd, posix=True)
        lexer.whitespace = " "
        lexer.whitespace_split = True
        lexer.escapedquotes = "'\""
        utils = Utilities(self.client, self.config, self.logger, room)
        try:
            raw_splitted = list(lexer)
        except ValueError as ex:
            self.logger.error(f"Failed to parse command: {raw_cmd}\n{ex}")
            await utils.send_message(f"#### Error\n\
Failed to parse command: {raw_cmd}")
        else:
            args = raw_splitted[1:]
            name = raw_splitted[0].lower()
            self.logger.debug(f"Parsed arguments: %s" % args)
            # Kwargs because I'm dumb.
            kwargs = {'self': self, 'room': room, 'event': event, 'utils': utils, 'args': args, 'prefix': prefix, 'name': name, 'cache': cache, 'raw': raw_cmd}
            if parent_command is None:
                parent_command = command
            self.logger.debug(f"Itering through commands list... (Commands list length: {len(parent_command.commands)})")
            cmd = parent_command.get_command(name)
            if cmd is not None:
                self.logger.debug(f"Found command: {cmd.name}, executing...")
                result = await cmd(**kwargs)
                if result is not None:
                    if result[0] == "request_cache":
                        self.logger.debug(f"Got cache request, caching... (Cache type: {result[1]}")
                        gen_kwargs = {
                            "room": room,
                            "event": event,
                            "prefix": prefix,
                            "raw_cmd": raw_cmd,
                            "parent_command": parent_command
                        }
                        self.caches[result[1]] = [None, gen_kwargs, True] 
            else:
                self.logger.debug(f"Command not found: {name}")
                unknown_cmd = command.get_command("_unknown_command", hidden = True)
                if unknown_cmd is not None:
                    await unknown_cmd(**kwargs)
                else:
                    self.logger.debug("You have not implemented a command to handle unknown commands, if this was your \
intention then you can ignore this warning message, otherwise please write a command with name '_unknown_command'")
        
    @command.Command(name="help", description="Show the help.")
    async def _help(self, utils, args, prefix, room, event, base_command = None, **kwargs):
        """Show the help text"""
        text = ""
        if base_command is None:
            base_command = command
            text += "### Available commands:\n"
        else:
            text += f"#### Available commands for `{base_command.name}`:\n"
        if len(args) == 0: 
            for v in base_command.commands:
                if not v.hidden:
                    text += f"+ `{v.name}` - {v.description}\n"
            await utils.send_message(text)
        else:
            for v in args:
                gen_kwargs = {
                    "room": room,
                    "event": event,
                    "prefix": prefix,
                    "raw_cmd": f"{v} help",
                    "parent_command": base_command
                }
                # Execute the command and let the parser do the rest.
                await self._process(**gen_kwargs)
        await utils.send_message(">I'm planning to rewrite the whole bot with a better core named 'Majo' (It'll be similar to discord.py), so\
the API will become usable for everyone, not just for myself.")

    @command.Command(name="_unknown_command", description="Raises when a command is not found", hidden=True)
    async def _unknown_command(self, utils, name, prefix, **kwargs):
        await utils.send_message(f"#### Error\n\
Unknown command: `{name}`, try `{prefix}help` to show all commands.")