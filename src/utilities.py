import asyncio
import random
import string
import mimetypes
import yt_dlp
from markdown import markdown
from typing import Optional
from aiopath import AsyncPath
from io import BytesIO
from PIL import Image
from nio import crypto
from nio import *

class Utilities:
    # Sometimes we dont need room but if we want to send message then room is required.
    def __init__(self, client: AsyncClient, config: dict, logger, room = None):
        self.client = client
        self.config = config
        self.room = room
        self.logger = logger

    # Message related.
    async def send_message(self, message):
        """
        Send a message to the current room.
        @param message: Message content in Markdown format
        """
        # Workaround for https://github.com/matrix-org/matrix-appservice-slack/issues/547
        content = {
            "msgtype": "m.text",
            "format": "org.matrix.custom.html", # Markdown support
            "formatted_body": markdown(message), # Markdown support
            "body": message,
        }
        await self.client.room_send(
            room_id=self.room.machine_name,
            message_type="m.room.message",
            content=content,
            ignore_unverified_devices = True
        )

    async def upload_file(self, file):
        """
        Upload a file to the current Matrix homeserver.
        @param file: The file to upload as an AsyncPath object.
        """
        if not isinstance(file, AsyncPath):
            file = AsyncPath(file)
        self.logger.debug("Uploading file: %s", file)
        async with file.open("r+b") as f:
            resp, _ = await self.client.upload(
                BytesIO(await f.read()),
                content_type=mimetypes.guess_type(file.name)[0],  # image/jpeg
                filename=file.name,
                filesize=(await file.stat()).st_size
            )
        if (isinstance(resp, UploadResponse)):
            self.logger.debug("File was uploaded successfully to server. ")
            return resp
        else:
            self.logger.debug(f"Failed to upload file. Failure response: {resp}")
            raise LocalTransportError("Failed to upload file.", resp)

    async def send_file(self, file, file_url = None, name = "Output", content = None, upload = True):
        """
        Send a file to the current room.
        @param file: The file to send as a string.
        @param file_url: The URL of the file (default is None).
        @param name: The name of the file (default is "Output").
        @param content: The dict content of the file message (default is None).
        @param upload: Upload the file to the homeserver (default is True).
        If the file_url is None, the content is None and upload is True, then the file will be uploaded to the homeserver before sending.
        """
        file = AsyncPath(file)
        if file_url is None and content is None and upload:
            file_url = (await self.upload_file(file)).content_uri
        if content is None:
            content = {
                "body": name + file.suffix,  # descriptive title
                "info": {
                    "size": (await file.stat()).st_size,
                    "mimetype": mimetypes.guess_type(file.name)[0]
                },
                "msgtype": "m.file",
                "url": file_url,
            }
        await self.client.room_send(
            room_id=self.room.machine_name,
            message_type="m.room.message",
            content=content,
            ignore_unverified_devices = True
        )

    async def send_image(self, file, file_url = None, name = "Output", upload = True):
        """
        Send a image to the current room.
        @param file: The file to send as a string.
        @param file_url: The URL of the file (default is None).
        @param name: The name of the file (default is "Output").
        @param content: The dict content of the file message (default is None).
        @param upload: Upload the file to the homeserver (default is True).
        If the file_url is None, the content is None and upload is True, then the file will be uploaded to the homeserver before sending.
        """
        width, height = None, None
        with Image.open(file) as im:
            width, height = im.size  # im.size returns (width,height) tuple
        image = AsyncPath(file)
        if file_url is None and upload:
            file_url = (await self.upload_file(image)).content_uri
        resp = await self.upload_file(image)
        content = {
            "body": name + image.suffix,  # descriptive title
            "info": {
                "size": (await image.stat()).st_size,
                "mimetype": mimetypes.guess_type(file)[0],
                "thumbnail_info": None,  # TODO
                "w": width,  # width in pixel
                "h": height,  # height in pixel
                "thumbnail_url": None,  # TODO
            },
            "msgtype": "m.image",
            "url": resp.content_uri,
        }
        await self.send_file(file, content=content)

    async def execute_process(self, *args):
        proc = await asyncio.create_subprocess_exec(*args, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
        stdout, stderr = await proc.communicate()
        self.logger.debug(f'[{proc} exited with {proc.returncode}]')
        if stdout:
            self.logger.debug(f'[stdout]\n{stdout.decode()}')
        if stderr:
            self.logger.debug(f'[stderr]\n{stderr.decode()}')

    # Generators (doesn't require async)
    def gen_random_str(self, len):
        return ''.join(random.choices(string.ascii_lowercase + string.digits, k=len))

    # Generate a help for commands.
    def gen_help(self, name, description, usage):
        return f"### `{name}`\n> {description}\n{usage}"

    def parse_download_url(self, url):
        self.logger.debug(f'parse_download_url: {url.split("/")[2:]}')
        return url.split("/")[2:]

    # copied some code from matrix-commander
    # wrapper for downloading all files encrypted/unencrypted
    async def download_file(self, event):
        media_mxc = event.url
        server, file_id = self.parse_download_url(media_mxc)
        rsp = await self.client.download(server, file_id)
        if isinstance(event, RoomEncryptedMedia):  # for all e2e media
            self.logger.debug("Decrypting attachment...")
            rsp = rsp.from_data(crypto.attachments.decrypt_attachment(
                rsp.body,
                event.key["k"],
                event.hashes[
                    "sha256"
                ],
                event.iv,
            ), event.mimetype)
        return rsp

    def ytdl_fetch_info(self, url, search = False):
        def get_best_media(audios, provider, is_video = False):
            if is_video:
                self.logger.debug("Running in Video mode...")
            ret_audio = None
            audio_quality = 0
            self.logger.debug(f"All media quality: {len(audios)}")
            for audio in audios:
                if (audio["video_ext"] == "none" and not is_video) or (audio["video_ext"] != "none" and is_video):
                    audio_q = int(''.join(i for i in audio["format"] if i.isdigit()))
                    self.logger.debug(f"Media format: {audio}")
                    self.logger.debug(f"Current media quality: {audio_q}, {audio['format']}")
                    rules = audio_q > audio_quality
                    if provider == "soundcloud":
                        # We prioritize the Opus encoder (which is lower bitrate than mp3 but still provide good quality
                        # so we check if the audio_q is lower than the previous quality (if the previous is 0 it'll set))
                        # since Soonudcloud only provide 128kbps mp3 and 64kbps Opus
                        rules = (audio_q < audio_quality or audio_quality == 0) and audio["protocol"] != "m3u8_native"
                    elif provider == "youtube" and not is_video:
                        # Use m4a instead of webm (Opus) because users want to download a music file directly instead
                        # of downloading a media file and having to convert it to a music file.
                        rules = (audio_q > audio_quality and audio["audio_ext"] == "m4a")
                    if rules:
                        audio_quality = audio_q
                        ret_audio = audio
                        self.logger.debug(f"Set {audio['format']} as the current best media quality.")
            return ret_audio
        options = {
            'format': 'bestvideo+bestaudio/best',
            'skip_download': True,
            'simulate': True,
            'logger': self.logger,
            'quiet': True
        }
        with yt_dlp.YoutubeDL(options) as ytdl:
            if search:
                media_info = ytdl.extract_info(f"ytsearch:{url}", download=False)["entries"][0]
            else:
                media_info = ytdl.extract_info(url, download=False)
            self.logger.debug(media_info)
            self.logger.debug(f'Total formats count: {len(media_info["formats"])}')
            self.logger.debug(f"Trying to parse to get the best audio possible...")
            best_audio = get_best_media(media_info["formats"], media_info["extractor"])
            best_video = get_best_media(media_info["formats"], media_info["extractor"], is_video = True)
            return media_info, best_audio, best_video

    def trust_devices(self, user_id: str, device_list: Optional[str] = None) -> None:
        """Trusts the devices of a user.

        If no device_list is provided, all of the users devices are trusted. If
        one is provided, only the devices with IDs in that list are trusted.

        Arguments:
            user_id {str} -- the user ID whose devices should be trusted.

        Keyword Arguments:
            device_list {Optional[str]} -- The full list of device IDs to trust
                from that user (default: {None})
        """

        self.logger.debug(f"{user_id}'s device store: {self.client.device_store[user_id]}")

        # The device store contains a dictionary of device IDs and known
        # OlmDevices for all users that share a room with us, including us.

        # We can only run this after a first sync. We have to populate our
        # device store and that requires syncing with the server.
        for device_id, olm_device in self.client.device_store[user_id].items():
            # if device_list and device_id not in device_list:
            #     # a list of trusted devices was provided, but this ID is not in
            #     # that list. That's an issue.
            #     self.logger.debug(f"Not trusting {device_id} as it's not in {user_id}'s pre-approved list.")
            #     continue

            self.client.verify_device(olm_device)
            self.logger.info(f"Trusting {device_id} from user {user_id}")
            